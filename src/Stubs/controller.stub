<?php

namespace {{namespace_prefix}}\{{class}}\Controllers;


use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Erpmonster\Http\Controller;
use {{namespace_prefix}}\{{class}}\Services\{{model_name}}Service;
use Illuminate\Validation\ValidationException;

class {{model_name}}Controller extends Controller
{
    /**
     * @var {{model_name}}Service
     */
    private $service;

    /**
     * Define validation before create and update
     *
     * @var array
     */

    private $validationRules = [
        'name' => 'required|max:128',
    ];

    public function __construct({{model_name}}Service $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     * @return JsonResponse
     */
    public function index()
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->service->get($resourceOptions);

        $parsedData = $this->parseData($data, $resourceOptions, 'data');

        return $this->response($parsedData);
    }

    /**
     * Display a selected  resource.
     * @param $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $resourceOptions = $this->parseResourceOptions();
        $data = $this->service->getById($id);
        $parsedData = $this->parseData($data, $resourceOptions, 'data');
        return $this->response($parsedData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->validationRules);

        $result = $this->service->create($request->all());

        return $this->response($result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->validationRules);

        $result = $this->service->update($id, $request->all());

        return $this->response($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $this->service->delete($id);

        return $this->response('Deleted', 201);
    }

    public function lookup(Request $request)
    {

        $keyField = $request->get('lookupKeyField');

        $displayField = $request->get('lookupDisplayField');

        $selectedId = $request->get('selectedId');

        $q = $request->get('q', '');

        $q = trim($q);

        $lookup = $this->service->lookup($keyField, $displayField, $selectedId, $q);

        return $this->response($lookup);

    }
}
